// rotatingfiles is a frontend for files rotating when they exceed a specific
// size of a specific age. It is designed to be used when you can't (or don't
// want) use logrotate.
package rotatingfiles

import "io"

// RotatingFiles is an interface for objects implementing the differents
// kind of files rotation.
type RotatingFiles interface {
	io.Writer
	Open() error
	Close() error
}
