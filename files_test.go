package rotatingfiles

import (
	"path/filepath"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNewFiles(t *testing.T) {
	Convey("Call newFiles with a path and a number of files to keep", t, func() {

		dirPath := filepath.Join("foo", "bar")
		f, _ := newFiles(filepath.Join(dirPath, "baz.log"), 123)
		Convey("It extract the directory", func() {
			So(f.directory, ShouldEqual, dirPath)
		})

		Convey("It extract the base filename", func() {
			So(f.mainFileName, ShouldEqual, "baz.log")
		})
	})
}

func TestOpen(t *testing.T) {
	Convey("Given a 'files' structure", t, withTestDirectory(func(tmpdir string) {

		mainFilePath := filepath.Join(tmpdir, "foo.log")
		f, _ := newFiles(mainFilePath, 1)
		Convey("Call Open create the file if it does not exists", func() {
			err := f.open()
			defer f.close()
			So(err, ShouldBeNil)
			So(f.currentFile, ShouldNotBeNil)
			So(fileExists(mainFilePath), ShouldBeTrue)
		})
	}))
}

func TestRotate(t *testing.T) {
	Convey("Given a 'files' structure", t, withTestDirectory(func(tmpdir string) {

		mainFilePath := filepath.Join(tmpdir, "foo.log")
		maxKeepedFiles := 3
		f, _ := newFiles(mainFilePath, maxKeepedFiles)
		err := f.open()
		panicIfError(err)
		defer f.close()

		Convey("Call rotate rename the current file", func() {
			content := "Dummy Content"
			f.currentFile.WriteString(content)
			panicIfError(f.rotate())
			newName := mainFilePath + ".1"
			So(fileExists(newName), ShouldBeTrue)
			So(checkContent(newName, content), ShouldBeTrue)
		})

		Convey("Call rotate create and open a new main file", func() {
			panicIfError(f.rotate())
			size, err := f.currentFile.Seek(0, 2)
			panicIfError(err)
			So(size, ShouldEqual, 0)
		})

		Convey("Call rotate increment numbers of old files", func() {
			contentA := "foo"
			f.currentFile.WriteString(contentA)
			panicIfError(f.rotate())
			contentB := "bar"
			f.currentFile.WriteString(contentB)
			panicIfError(f.rotate())
			f.currentFile.WriteString("baz")

			fileNameA := mainFilePath + ".2"
			So(fileExists(fileNameA), ShouldBeTrue)
			So(checkContent(fileNameA, contentA), ShouldBeTrue)

			fileNameB := mainFilePath + ".1"
			So(fileExists(fileNameB), ShouldBeTrue)
			So(checkContent(fileNameB, contentB), ShouldBeTrue)
		})

		Convey("Call rotate keep only 'keepedVersions' files (including current)", func() {
			for i := 0; i < maxKeepedFiles; i++ {
				panicIfError(f.rotate())
			}
			fileList, err := filepath.Glob(f.mainFilePath + "*")
			panicIfError(err)
			So(len(fileList), ShouldEqual, maxKeepedFiles)
		})
	}))
}
