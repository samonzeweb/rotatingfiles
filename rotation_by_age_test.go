package rotatingfiles

import (
	"path/filepath"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRotationBAge(t *testing.T) {
	Convey("Given a RotationByAge", t, withTestDirectory(func(tmpdir string) {

		mainFilePath := filepath.Join(tmpdir, "foo.log")
		maxAge := 24 * time.Hour
		f, err := NewRotationByAge(mainFilePath, 2, maxAge)
		panicIfError(err)
		err = f.Open()
		panicIfError(err)
		defer f.Close()

		Convey("It don't rotate if the age is under the limit", func() {
			f.Write([]byte("Dummy content"))
			So(fileExists(mainFilePath), ShouldBeTrue)
			otherFile := mainFilePath + ".1"
			So(fileExists(otherFile), ShouldBeFalse)
		})

		Convey("It rotate when the age reach the limit", func() {
			// Use a TARDIS to test a so simple case
			f.originTime = f.originTime.Add(-maxAge - 1)
			f.Write([]byte("Dummy content"))
			So(fileExists(mainFilePath), ShouldBeTrue)
			otherFile := mainFilePath + ".1"
			So(fileExists(otherFile), ShouldBeTrue)
		})

	}))
}
