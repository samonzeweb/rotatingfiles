package rotatingfiles

import (
	"errors"
	"os"
	"syscall"
	"time"
)

func getFileCreationTime(filePath string) (time.Time, error) {
	var ctime time.Time

	fileInfo, err := os.Stat(filePath)
	if err != nil {
		return ctime, err
	}

	stat, ok := fileInfo.Sys().(*syscall.Win32FileAttributeData)
	if !ok {
		return ctime, errors.New("File creation time is unavailable")
	}

	ctime = time.Unix(0, stat.CreationTime.Nanoseconds())
	return ctime, nil
}
