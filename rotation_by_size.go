package rotatingfiles

import "os"

// RotationBySize is an implementation of RotatingFiles for fixed size files.
type RotationBySize struct {
	*files
	currentSize int64
	maxSize     int64
}

// NewRotationBySize create a new RotationBySize object
func NewRotationBySize(mainFilePath string, keepedVersions int, maxSize int64) (*RotationBySize, error) {
	f, err := newFiles(mainFilePath, keepedVersions)
	if err != nil {
		return nil, err
	}

	rf := &RotationBySize{files: f}
	rf.currentSize = 0
	rf.maxSize = maxSize
	return rf, nil
}

// Open opens the main log file with mutex
func (rf *RotationBySize) Open() error {
	rf.mutex.Lock()
	defer rf.mutex.Unlock()

	err := rf.open()
	if err != nil {
		return err
	}

	fileInfo, err := os.Stat(rf.mainFilePath)
	if err != nil {
		return err
	}

	rf.currentSize = fileInfo.Size()
	return nil
}

// Write implements io.Writer
func (rf *RotationBySize) Write(p []byte) (n int, err error) {
	rf.mutex.Lock()
	defer rf.mutex.Unlock()

	newSize := rf.currentSize + int64(len(p))
	if newSize > rf.maxSize {
		if err := rf.rotate(); err != nil {
			return 0, err
		}
		rf.currentSize = 0
	}
	writen, err := rf.currentFile.Write(p)
	rf.currentSize += int64(writen)
	return writen, err
}
