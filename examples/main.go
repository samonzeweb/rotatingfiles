package main

import (
	"fmt"
	"io"
	"log"
	"time"

	"bitbucket.org/samonzeweb/rotatingfiles"
)

var buffer = []byte("The quick brown fox jumps over the lazy dog\n")

func main() {
	fmt.Println("Rotation by size...")
	rotationBySize()
	fmt.Println("Rotation by age...")
	rotationByAge()
	fmt.Println("Rotating logger...")
	rotatingLogger()
}

func rotationBySize() {
	rbs, err := rotatingfiles.NewRotationBySize("foo_size.log", 3, int64(1024))
	if err != nil {
		panic(err)
	}
	err = rbs.Open()
	if err != nil {
		panic(err)
	}
	defer rbs.Close()

	writeContent(rbs, buffer, 50)
}

func rotationByAge() {
	rba, err := rotatingfiles.NewRotationByAge("foo_age.log", 3, time.Second)
	if err != nil {
		panic(err)
	}
	err = rba.Open()
	if err != nil {
		panic(err)
	}
	defer rba.Close()

	for i := 0; i < 3; i++ {
		writeContent(rba, buffer, 50)
		time.Sleep(time.Second)
	}
}

func writeContent(f io.Writer, content []byte, times int) {
	for i := 0; i < times; i++ {
		_, err := f.Write(buffer)
		if err != nil {
			panic(err)
		}
	}
}

func rotatingLogger() {
	var fileForLogger rotatingfiles.RotatingFiles

	fileForLogger, err := rotatingfiles.NewRotationBySize("foo_logger_size.log", 3, int64(64))
	if err != nil {
		panic(err)
	}
	err = fileForLogger.Open()
	if err != nil {
		panic(err)
	}
	defer fileForLogger.Close()

	log.SetOutput(fileForLogger)
	for i := 0; i < 10; i++ {
		log.Print("I am a logger")
		log.Print("Writing to an io.Writer")
		log.Print("Which is a group of rotating files")
	}
}
