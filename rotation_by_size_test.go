package rotatingfiles

import (
	"path/filepath"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRotationBySize(t *testing.T) {
	Convey("Given a RotationBySize", t, withTestDirectory(func(tmpdir string) {

		mainFilePath := filepath.Join(tmpdir, "foo.log")
		maxSize := int64(20)
		f, err := NewRotationBySize(mainFilePath, 2, maxSize)
		panicIfError(err)
		err = f.Open()
		panicIfError(err)
		defer f.Close()

		Convey("It don't rotate if the size is under the limit", func() {
			f.Write([]byte("0123456789"))
			So(fileExists(mainFilePath), ShouldBeTrue)
			otherFile := mainFilePath + ".1"
			So(fileExists(otherFile), ShouldBeFalse)
		})

		Convey("It rotate when the size reach the limit", func() {
			f.Write([]byte("012345678901234567890"))
			So(fileExists(mainFilePath), ShouldBeTrue)
			otherFile := mainFilePath + ".1"
			So(fileExists(otherFile), ShouldBeTrue)
		})

		Convey("It considers the size of an existing main file when opening it", func() {
			f.Write([]byte("01234567890123456789"))
			f.Close()
			f.Open()
			f.Write([]byte("0"))
			So(fileExists(mainFilePath), ShouldBeTrue)
			otherFile := mainFilePath + ".1"
			So(fileExists(otherFile), ShouldBeTrue)
		})

	}))
}
