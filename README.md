# rotatingfiles

It's a Go package managing files rotation. It is designed to be used for log files, or similar uses, when using logrotate is not an option.

It allows to create virtual files rotating by size, or by age.

It was tested on Windows, Linux and OS X.

# Install

```
go get bitbucket.org/samonzeweb/rotatingfiles
```

# Usage

## Overview

You can create two kings of rotating files, with too dedicated types :

* `RotationBySize` : when the current file reach the size limit (bytes), the rotation occurs.
* `RotationByAge` : when the current file reach the age limit (time.Duration), the rotation occurs.

Both have similar uses and implement the `RotatingFiles` interface. As they also implement the `io.Writer` interface,  they can be used as a logger :

```
rbs, err := rotatingfiles.NewRotationBySize("foo_logger_size.log", 3, int64(64))
if err != nil {
  panic(err)
}
err = rbs.Open()
if err != nil {
  panic(err)
}
defer rbs.Close()

log.SetOutput(rbs)
for i := 0; i < 10; i++ {
  log.Print("I am a logger")
  log.Print("Writing to an io.Writer")
  log.Print("Which is a group of rotating files")
}
```

See the content of `examples/main.go` for all examples. It's really a simple piace of code.

## Concurrency

The package is designed to be safe when using concurrent goroutines. This has yet to be proven by experience ;)

# Development

This library only need [GoConvey](http://goconvey.co/) to run the tests. Install it with :

```
go get github.com/smartystreets/goconvey
```

# Licence

Released under the MIT License, see `LICENSE.txt` for more informations.
