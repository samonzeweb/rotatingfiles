package rotatingfiles

import (
	"io/ioutil"
	"os"
)

func panicIfError(err error) {
	if err != nil {
		panic(err)
	}
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return (err == nil)
}

func checkContent(path string, expectedContent string) bool {
	content, err := ioutil.ReadFile(path)
	panicIfError(err)
	return (string(content) == expectedContent)
}

func withTestDirectory(f func(string)) func() {
	return func() {
		var tmpdir string
		var err error

		if tmpdir, err = ioutil.TempDir("", "test_rotatingfile"); err != nil {
			panic(err)
		}

		f(tmpdir)

		if err = os.RemoveAll(tmpdir); err != nil {
			panic(err)
		}
	}
}
