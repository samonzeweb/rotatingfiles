package rotatingfiles

import (
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"sync"
)

// files : describe common data for rotating files
type files struct {
	// General informations (don't change)
	mainFilePath   string
	directory      string
	mainFileName   string
	keepedVersions int
	// Current state
	currentFile *os.File
	// Concurrency
	mutex sync.Mutex
}

// newFiles creates a new files object
func newFiles(mainFilePath string, keepedVersions int) (*files, error) {
	f := &files{}
	f.mainFilePath = mainFilePath
	f.directory = filepath.Dir(mainFilePath)
	f.mainFileName = filepath.Base(mainFilePath)
	f.keepedVersions = keepedVersions
	return f, nil
}

// open : open the main log file without mutex
func (f *files) open() error {
	var err error

	if f.currentFile != nil {
		return nil
	}
	flags := os.O_CREATE | os.O_WRONLY | os.O_APPEND
	// TODO : add perms parameters
	f.currentFile, err = os.OpenFile(f.mainFilePath, flags, 0600)
	return err
}

// Close closes the main log file with mutex
func (f *files) Close() error {
	f.mutex.Lock()
	defer f.mutex.Unlock()

	return f.close()
}

// close closes the main log file without mutex
func (f *files) close() error {
	if f.currentFile != nil {
		err := f.currentFile.Close()
		f.currentFile = nil
		return err
	}
	return nil
}

// rotate rename all existing files, delete the oldest if there are too many versions.
// It does not manage concurrency, because it has to be done by the caller.
func (f *files) rotate() error {
	var err error

	// Close the current log file
	err = f.close()
	if err != nil {
		return err
	}

	// Rename all files (except the current) beginning with the oldest
	// The Glob syntax can catch too many files, the Regexp will
	// filter the files and extract the version number.
	// Exemple : [...]/mylogfile.1.copy
	fileList, err := filepath.Glob(f.mainFilePath + ".[0-9]*")
	sort.Sort(sort.Reverse(sort.StringSlice(fileList)))
	numberExtractor, err := regexp.Compile("\\.([0-9]+)$")
	if err != nil {
		return err
	}
	allLogFiles := make([]string, 0, len(fileList))
	for _, filePath := range fileList {
		matchingStrings := numberExtractor.FindStringSubmatch(filePath)
		if len(matchingStrings) == 2 {
			number, err := strconv.Atoi(matchingStrings[1])
			if err == nil {
				newFilePath := f.mainFilePath + "." + strconv.Itoa(number+1)
				if err := os.Rename(filePath, newFilePath); err != nil {
					return err
				}
				allLogFiles = append(allLogFiles, newFilePath)
			}
		}
	}

	// Rename current file
	logFileNumberOne := f.mainFilePath + ".1"
	if err := os.Rename(f.mainFilePath, logFileNumberOne); err != nil {
		return err
	}
	allLogFiles = append(allLogFiles, logFileNumberOne)

	// Delete too old files (the new main file isn't already created)
	numberOfFilesToDelete := len(allLogFiles) - f.keepedVersions + 1
	for numberOfFilesToDelete > 0 {
		if err := os.Remove(allLogFiles[0]); err != nil {
			return err
		}
		numberOfFilesToDelete--
		allLogFiles = allLogFiles[1:]
	}

	// Open a new log file
	err = f.open()
	return err
}
