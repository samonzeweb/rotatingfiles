package rotatingfiles

import "time"

// RotationByAge is an implementation of RotatingFiles for fixed age files.
type RotationByAge struct {
	*files
	originTime time.Time
	maxAge     time.Duration
}

// NewRotationByAge create a new RotationByAge object
func NewRotationByAge(mainFilePath string, keepedVersions int, maxAge time.Duration) (*RotationByAge, error) {
	f, err := newFiles(mainFilePath, keepedVersions)
	if err != nil {
		return nil, err
	}

	rf := &RotationByAge{files: f}
	// originTime will be set when opening the file
	rf.maxAge = maxAge
	return rf, nil
}

// Open opens the main log file with mutex
func (rf *RotationByAge) Open() error {
	rf.mutex.Lock()
	defer rf.mutex.Unlock()

	err := rf.open()
	if err != nil {
		return err
	}

	rf.originTime, err = getFileCreationTime(rf.mainFilePath)
	return err
}

// Write implements io.Writer
func (rf *RotationByAge) Write(p []byte) (n int, err error) {
	rf.mutex.Lock()
	defer rf.mutex.Unlock()

	now := time.Now()
	currentAge := now.Sub(rf.originTime)
	if currentAge > rf.maxAge {
		if err := rf.rotate(); err != nil {
			return 0, err
		}
		rf.originTime = now
	}

	writen, err := rf.currentFile.Write(p)
	return writen, err
}
